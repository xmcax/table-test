angular
  .module('app')
  .component('table', {
    templateUrl: 'app/main.html',
    controller: MainController
  });

MainController.$inject = ['$log', '$http', 'TimeService'];

function MainController($log, $http, TimeService) {
  const vm = this;

  $log.info('controller initialization started : ', TimeService.getMsFromStart());

  $http.get('MOCK_DATA.json')
    .then(response => {
      vm.data = response.data;
      vm.displayed = response.data;
      $log.info('test data loaded : ', TimeService.getMsFromStart());
    });
}
