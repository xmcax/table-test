(function() {
  'use strict';

  angular
    .module('app')
    .directive('postRepeatDirective', PostRepeatDirective);

  PostRepeatDirective.$inject = ['$timeout', '$log', 'TimeService'];

  function PostRepeatDirective($timeout, $log, TimeService) {
    return {
      restrict: 'A',
      link: function (scope) {
        if (scope.$last) {
          $timeout(function () {
            $log.debug("DOM rendered : " + TimeService.getMsFromStart());
          });
        }
      }
    };
  }
})();
