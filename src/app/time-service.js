(function() {
  'use strict';

  angular
    .module('app')
    .service('TimeService', TimeService);

  TimeService.$inject = [];

  function TimeService() {
    const start = Date.now();

    return {
      getMsFromStart : getMsFromStart
    };

    function getMsFromStart() {
      return Date.now() - start;
    }
  }

})()
